FROM node:20-alpine AS builder

LABEL maintainer="RENFIX"

WORKDIR /app

COPY package.json ./

RUN npm install

COPY . .

CMD ["node", "app"]