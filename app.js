const express = require("express");
const socket = require("socket.io");
const bodyParser = require("body-parser");
const cors = require("cors");
const axios = require('axios');
const dotenv = require('dotenv');

//config env
dotenv.config({ path: './config.env' })

// App setup
const PORT = 5000;
const app = express();
const server = app.listen(PORT, function () {
  console.log(`Listening on port ${PORT}`);
  console.log(`http://localhost:${PORT}`);
});

//config baseURL thaielder API 
axios.defaults.baseURL = process.env.THAIELDER_API;

//registering cors
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*")
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization")
  next()
});

// Socket setup
const io = socket(server);

io.on("connection", function (socket) {

  console.log("Made socket connection");
  socket.on("connect", () => {
    //socket.emit("newuser", "hello connect");
    console.log("Client begins to connect");
  });

  //socket.emit("newuser", ["ok",{"id":1,"name":"tom"}]);
  socket.on("ems_response_mobile", function (data) {

    const obj = JSON.parse(data);

    if (obj.actions == "cancel") {
      const obj_res = {
        confirm_contact: obj.confirm_contact,// สถานะการตอบรับ
        response_status: "ok"
      };
      //emit ไปยัง chanel emscall_response ทุกๆเครื่องที่เปิดอยู่
      io.sockets.emit("emscall_response", 'ok', obj_res);
    }

    if (obj.actions == "agree") {

      var ems_data = {
        _id: obj.emscall_id,
        create_by: "1669",
        create_date: fnDateCurrent()+" "+fnTimeCurrent(),
        created_at: fnDateCurrent()+" "+fnTimeCurrent(),
        ems_accident_location: obj.location_patient,
        ems_address: "",
        ems_amphoe: "",
        ems_caller: "",
        ems_changwat: "",
        ems_chronic: obj.disease_chronic,
        ems_latitude: obj.latitude,
        ems_longitude: obj.longitude,
        ems_notify: obj.detail_notify,
        ems_postcode: "",
        ems_soi: "-",
        ems_tambon: "",
        ems_tel: obj.tel,
        update_by: "1669",
        update_date: fnDateCurrent()+" "+fnTimeCurrent(),
        updated_at: fnDateCurrent()+" "+fnTimeCurrent(),
        ems_status: obj.confirm_contact, //1 = ยกเลิกคำร้อง, 2 = กำลังดำเนินการ,  3 = ได้รับการแจ้งก่อนหน้า/แจ้งซ้ำซ้อน,  4 = ดำเนินการเสร็จสิ้น
        ems_status_detail: obj.ems_status_detail,
        ems_changwat: obj.ems_changwat,
        provinceId: obj.provinceId,
        ems_amphoe: obj.ems_amphoe,
        amphurId: obj.amphurId,
        ems_tambon: obj.ems_tambon,
        tambonId: obj.tambonId
      };

      //console.log(obj.emscall_id);

      const auth = {
        username: process.env.API_USERNAME,
        password: process.env.API_PASSWORD
      }

      // Optionally the request above could also be done as
      axios.post('login-auth', auth)
        .then(function (response) {
          //console.log(response.data.accessToken);
          if (response.data.accessToken) {
            var token = response.data.accessToken;
            let config = {
              headers: {
                'Authorization': 'Bearer ' + response.data.accessToken
              }
            }
            axios.post('er_call/ems_insert',
              ems_data,
              config)
              .then(function (response) {
                const obj_res = {
                  confirm_contact: obj.confirm_contact,// สถานะการตอบรับ
                  secure: obj.emsname, //หน่วยกู้ชีพ
                  response_status: "บันทึกการตอบรับ สำเร็จ"
                };
                //emit ไปยัง chanel emscall_response ทุกๆเครื่องที่เปิดอยู่
                io.sockets.emit("emscall_response", 'ok', obj_res);
                //console.log(response);
              })
              .catch(function (error) {
                const obj_res = {
                  response_status: error.response.data.error,
                };
                io.sockets.emit("emscall_response", 'ok', obj_res);
                console.log(error.response.data.error);
              })
          }
        })
        .catch(function (error) {
          console.log(error);
        })
        .finally(function () {
          // always executed
        });
      //socket.emit("emscall", "hello user");
      //console.log(data)
      //socket.userId = data;
      //activeUsers.add(data);
      //io.emit("new user", [...activeUsers]);

    }

  });

  socket.on("emscall", function (data) {
    console.log("log: " + data)
    //socket.userId = data;
    //activeUsers.add(data);
    //io.emit("new user", [...activeUsers]);
  });

  socket.on("disconnect", () => {
    console.log("user disconnected");
    //activeUsers.delete(socket.userId);
    //io.emit("user disconnected", socket.userId);
  });
});


app.post('/ems-save', (req, res) => {

  //emit ไปยัง mobile ใน chanel 
  //io.sockets.emit("ems-response", 'ok', data_obj);

})

app.post('/ems-call', (req, res) => {

  //er_call/ems_insert
  ///login-auth
  const data_obj = {
    emscall_id: req.body.emscall_id, //id auto incrument
    detail_notify: req.body.detail_notify, //diag ข้อมูลเพิ่มเติม
    location_patient: req.body.location_patient, //สถานที่พักอาศัยจริง
    disease_chronic: req.body.disease_chronic, //โรคประจำตัว
    tel: req.body.tel, //เบอร์โทร
    reporter: req.body.reporter, //ชื่อแจ้งเหตุ
    age: req.body.age, //อายุ
    fullname: req.body.fullname, //ชื่อ-สกุล (ผู้ป่วย)
    latitude: req.body.latitude, //ละจิจุด
    longitude: req.body.longitude, //ลองจิจุด
    confirm_contact: req.body.confirm_contact, //สถานะตอบกลับจากปลายทาง
    ems_changwat: req.body.ems_changwat,
    provinceId: req.body.provinceId,
    ems_amphoe: req.body.ems_amphoe,
    amphurId: req.body.amphurId,
    ems_tambon: req.body.ems_tambon,
    tambonId: req.body.tambonId,
  };

  //emit ไปยัง chanel newuser ทุกๆเครื่องที่เปิดอยู่
  io.sockets.emit("emscall", 'ok', data_obj);

  //res.send({"result":"ok"})
  res.send(data_obj)

})

const fnDateCurrent = () => {

  // current timestamp in milliseconds
  //let ts = Date.now();

  //let date_ob = new Date(ts);
  var date_ob = convertTZ(new Date(), "Asia/Jakarta")
  let date = date_ob.getDate();
  let month = date_ob.getMonth() + 1;
  let year = date_ob.getFullYear();
  
  return year + "-" + month + "-" + date;

}

const fnTimeCurrent = () => {

  // current timestamp in milliseconds
  let today = convertTZ(new Date(), "Asia/Jakarta")
  // current hours
  let hours = today.getHours();
  //let hours = today.getUTCHours();
  // current minutes
  let minutes = today.getMinutes();
  
  return hours + ":" + minutes+ ":00";
  //res.status(201).send(year + "-" + month + "-" + date);
}

const convertTZ = (date, tzString) => {
  return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: tzString}));   
}
